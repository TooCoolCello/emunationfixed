#pragma once
#include <string>
#include <vector>
#include "Armor.h"
#include "Weapons.h"
#include "Party.h"

struct emu {
	std::string name;
	classes emuClass;
	int level;
	double health;
	double strength;
	double speed;
	std::map<std::string, int> emuStats;
	std::vector<weapon> equippedWeapons;
	std::vector<armor> equippedArmor;
	bool isShieldEquipped;
	std::vector<shield> equippedShield;
	std::vector<damageTypes> weaknesses;
	std::vector<damageTypes> resistances;
	std::vector<damageTypes> immunities;
	double AP;
};

// Elemental Emus | Name | Type | Level | Health | Strength | Speed | Stats | Weapons | Armor | Shield? | Shields |Weaknesses | Ressistances | Immunities
emu englishJohnny = { "English Johnny", classes::WARLOCK, 1, 85, 1, 30, {{"Strength", 8}, {"Dexterity", 8}, {"Constitution", 8}, {"Intelligence", 8}, {"Wisdom", 8}, {"Charisma", 8}}, {dagger}, {leatherBoots, leatherChestplate, leatherGloves, leatherHelmet, leatherLeggings}, false, {}, {damageTypes::LIGHTING}, {}, {} };
emu jugman = { "Jug Man", classes::WIZARD, 1, 85, 1, 30, {{"Strength", 8}, {"Dexterity", 8}, {"Constitution", 8}, {"Intelligence", 8}, {"Wisdom", 8}, {"Charisma", 8}}, {dagger}, {leatherBoots, leatherChestplate, leatherGloves, leatherHelmet, leatherLeggings}, false, {},  {}, {}, {} };
emu grunt = { "Grunt", classes::PALADIN, 3, 85, 1, 30, {{"Strength", 8}, {"Dexterity", 8}, {"Constitution", 8}, {"Intelligence", 8}, {"Wisdom", 8}, {"Charisma", 8}}, {dagger}, {chainBoots, chainChestplate, chainGauntlets, chainHelmet, chainLeggings}, false, {},  {}, {}, {} };

std::vector<emu> emuVector{
	englishJohnny,
	jugman,
	grunt,
};









//emu philSwift = { "Phil Swift", classes::PHILSWIFT, 20, 100, 5, 50, plateArmor, {damageTypes::SLASHING, damageTypes::PIERCING, damageTypes::FIRE}, {damageTypes::POISON, damageTypes::ACID, damageTypes::BLUDGEONING, damageTypes::COLD, damageTypes::FORCE, damageTypes::LIGHTING, damageTypes::NECROTIC, damageTypes::PIERCING, damageTypes::PSYCHIC, damageTypes::RADIANT, damageTypes::THUNDER}, {} };

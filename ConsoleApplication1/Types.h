#pragma once

enum class damageTypes {
	ACID,
	BLUDGEONING,
	COLD,
	FIRE,
	FORCE,
	LIGHTING,
	NECROTIC,
	PIERCING,
	POISON,
	PSYCHIC,
	SLASHING,
	THUNDER,
	RADIANT,
	HEAL,
	EFFECT,
};

enum class statusEffects {
	BURNING,
	PARALYZED,
	HALUCINATING,
	SLEEPING,
	BLEEDING,
	DIZZY,
	EXHAUSTION,
	CHARMED,
	PRONE,
	FRIGHTENED,
	NA,
};

enum class armorMaterials {
	PADDED,
	LEATHER,
	HIDE,
	SCALE,
	CHAINMAIL,
	PLATE,
	SHIELD,
};
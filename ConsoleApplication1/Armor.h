#pragma once
#include <vector>
#include <string>
#include "Types.h"

enum class armorPieces {
	HELMET,
	CHESTPLATE,
	GAUNTLETS,
	LEGGINGS,
	BOOTS,
	SHIELD,
};
enum class armorTypes {
	LIGHT,
	MEDIUM,
	HEAVY,
	SHIELD,
};

struct armor {
	std::string name;
	armorPieces piece;
	armorMaterials material;
	armorTypes type;
	double armorPoints;
	double durability;
	bool initiativeDisadvantage;
};
struct shield {
	std::string name;
	armorPieces piece;
	armorTypes type;
	int armorPoints;
	double durability;
};

// Padded Armor | Name | Piece | Material | Armor Points | Durability | Disadvatage to Initiative
armor paddedHelmet = { "Leather Helmet", armorPieces::HELMET, armorMaterials::PADDED, armorTypes::LIGHT, 2, 225, true };
armor paddedChestplate = { "Padded Chestplate", armorPieces::CHESTPLATE, armorMaterials::PADDED, armorTypes::LIGHT, 3, 275, true };
armor paddedLeggings = { "Padded Chestplate", armorPieces::CHESTPLATE, armorMaterials::PADDED, armorTypes::LIGHT, 3, 250, true };
armor paddedGloves = { "Padded Gloves", armorPieces::GAUNTLETS, armorMaterials::PADDED, armorTypes::LIGHT, 1, 175, true };
armor paddedBoots = { "Padded Boots", armorPieces::BOOTS, armorMaterials::PADDED, armorTypes::LIGHT, 2, 225, true };

// Leather Armor | Name | Piece | Material | Armor Points | Durability | Disadvatage to Initiative 
armor leatherHelmet = { "Leather Helmet", armorPieces::HELMET, armorMaterials::LEATHER, armorTypes::LIGHT, 2, 250, false };
armor leatherChestplate = { "Leather Chestplate", armorPieces::CHESTPLATE, armorMaterials::LEATHER, armorTypes::LIGHT, 3, 300, false };
armor leatherLeggings = { "Leather Chestplate", armorPieces::CHESTPLATE, armorMaterials::LEATHER, armorTypes::LIGHT, 3, 275, false };
armor leatherGloves = { "Leather Gloves", armorPieces::GAUNTLETS, armorMaterials::LEATHER, armorTypes::LIGHT, 1, 200, false };
armor leatherBoots = { "Leather Boots", armorPieces::BOOTS, armorMaterials::LEATHER, armorTypes::LIGHT, 2, 250, false };

// Hide Armor | Name | Piece | Material | Armor Points | Durability | Disadvatage to Initiative
armor hideHelmet = { "Hide Helmet", armorPieces::HELMET, armorMaterials::HIDE, armorTypes::MEDIUM, 2, 275, false };
armor hideChestplate = { "Hide Chestplate", armorPieces::CHESTPLATE, armorMaterials::HIDE, armorTypes::MEDIUM, 4, 325, false };
armor hideLeggings = { "Hide Leggings", armorPieces::LEGGINGS, armorMaterials::HIDE, armorTypes::MEDIUM, 3, 300, false };
armor hideGloves = { "Hide Gloves", armorPieces::LEGGINGS, armorMaterials::HIDE, armorTypes::MEDIUM, 1, 225, false };
armor hideBoots = { "Hide Boots", armorPieces::BOOTS, armorMaterials::HIDE, armorTypes::MEDIUM, 2, 275, false };

// Scale Armor | Name | Piece | Material | Armor Points | Durability | Disadvatage to Initiative
armor scaleHelmet = { "Scale Helmet", armorPieces::HELMET, armorMaterials::SCALE, armorTypes::MEDIUM, 3, 300, true };
armor scaleChestplate = { "Scale Chestplate", armorPieces::CHESTPLATE, armorMaterials::SCALE, armorTypes::MEDIUM, 4, 375, true };
armor scaleLeggings = { "Scale Leggings", armorPieces::LEGGINGS, armorMaterials::SCALE, armorTypes::MEDIUM, 3, 350, true };
armor scaleGauntlets = { "Scale Gauntlets", armorPieces::LEGGINGS, armorMaterials::SCALE, armorTypes::MEDIUM, 2, 275, true };
armor scaleBoots = { "Scale Boots", armorPieces::BOOTS, armorMaterials::SCALE, armorTypes::MEDIUM, 2, 300, true };

// Chain Mail Armor | Name | Piece | Material | Armor Points | Durability | Disadvatage to Initiative
armor chainHelmet = { "Chain Mail Helmet", armorPieces::HELMET, armorMaterials::CHAINMAIL, armorTypes::HEAVY, 3, 475, true };
armor chainChestplate = { "Chain Mail Chestplate", armorPieces::CHESTPLATE, armorMaterials::CHAINMAIL, armorTypes::HEAVY, 4, 525, true };
armor chainLeggings = { "Chain Mail Leggings", armorPieces::LEGGINGS, armorMaterials::CHAINMAIL, armorTypes::HEAVY, 4, 500, true };
armor chainGauntlets = { "Chain Mail Gauntlets", armorPieces::LEGGINGS, armorMaterials::CHAINMAIL, armorTypes::HEAVY, 2, 425, true };
armor chainBoots = { "Chain Mail Boots", armorPieces::BOOTS, armorMaterials::CHAINMAIL, armorTypes::HEAVY, 3, 475, true };

// Plate Armor | Name | Piece | Material | Armor Points | Durability | Disadvatage to Initiative
armor plateHelmet = { "Plate Helmet", armorPieces::HELMET, armorMaterials::PLATE, armorTypes::HEAVY, 4, 500, true };
armor plateChestplate = { "Plate Chestplate", armorPieces::CHESTPLATE, armorMaterials::PLATE, armorTypes::HEAVY, 5, 550, true };
armor plateLeggings = { "Plate Leggings", armorPieces::LEGGINGS, armorMaterials::PLATE, armorTypes::HEAVY, 4, 525, true };
armor plateGauntlets = { "Plate Gauntlets", armorPieces::LEGGINGS, armorMaterials::PLATE, armorTypes::HEAVY, 2, 450, true };
armor plateBoots = { "Plate Boots", armorPieces::BOOTS, armorMaterials::PLATE, armorTypes::HEAVY, 3, 500, true };

shield starterShield = { "Starter Shield", armorPieces::SHIELD, armorTypes::SHIELD, 1, 200 };
shield woodenShield = { "Wooden Shield", armorPieces::SHIELD, armorTypes::SHIELD, 1, 250 };
shield ironShield = { "Iron Shield", armorPieces::SHIELD, armorTypes::SHIELD, 2, 350 };
shield steelShield = { "Steel Shield", armorPieces::SHIELD, armorTypes::SHIELD, 3, 500 };

/*
Each armor point (AP) reduces the damage you take by 3%, You can have a max of 20 armor points reducing the damage you take by 60%
*/
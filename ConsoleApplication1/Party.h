#pragma once
#include <string>
#include <vector>
#include <array>
#include <map>
#include "Armor.h"
#include "Weapons.h"

enum class classes {
	BARBARIAN,
	BARD,
	CLERIC,
	DRUID,
	FIGHTER,
	MONK,
	PALADIN,
	RANGER,
	ROGUE,
	WIZARD,
	WARLOCK,
	SORCEROR,
	SHIELDMASTER,
	PHILSWIFT
};
enum class races {
	DRAGONEBORN,
	DWARF,
	ELF,
	GNOME,
	HALFELF,
	HALFORC,
	HALFLING,
	HUMAN,
	TIEFLING,
	EMU,
};

struct partyMember {
	std::string name;
	classes playerClass;
	races playerRace;
	int level;
	double hp;
	double speed;
	int proficiency;
	std::map<std::string, std::map<std::string, int>> stats;
	std::vector<weapon> equippedWeapons;
	std::vector<armor> equippedArmor;
	bool isShieldEquipped;
	std::vector<shield> equippedShield;
	double AP;
};

partyMember BrarackOrbrama{ "Brarack Orbrama", classes::BARBARIAN, races::DWARF, 1, 12, 25, 0,  {{"Strength", {{"Strength", 8}, {"Athletics", 0}}}, {"Dexterity", {{"Dexterity", 8}, {"Acrobatics", 0}, {"Sleight of Hand", 0}, {"Stealth", 0}}}, {"Intelligence", {{"Intelligence", 8}, {"Arcana", 0}, {"History", 0}, {"Investigation", 0}, {"Nature", 0}, {"Religion", 0}}}, {"Wisdom", {{"Wisdom", 8}, {"Animal Handling", 0}, {"Insight", 0}, {"Medicine", 0}, {"Perception", 0}, {"Survival", 0}}}, {"Charisma", {{"Charisma", 8}, {"Deception", 0}, {"Intimidation", 0}, {"Performance", 0}, {"Persuasion", 0}}}}, {club,club}, {plateHelmet, plateChestplate, plateGauntlets, plateLeggings, plateBoots}, true, {woodenShield}, 0 };
partyMember JhananWhack{ "Jhanan Whack", classes::WARLOCK, races::HALFORC, 1, 100, 30, 0, {{"Strength", {{"Strength", 8}, {"Athletics", 0}}}, {"Dexterity", {{"Dexterity", 8}, {"Acrobatics", 0}, {"Sleight of Hand", 0}, {"Stealth", 0}}}, {"Intelligence", {{"Intelligence", 8}, {"Arcana", 0}, {"History", 0}, {"Investigation", 0}, {"Nature", 0}, {"Religion", 0}}}, {"Wisdom", {{"Wisdom", 8}, {"Animal Handling", 0}, {"Insight", 0}, {"Medicine", 0}, {"Perception", 0}, {"Survival", 0}}}, {"Charisma", {{"Charisma", 8}, {"Deception", 0}, {"Intimidation", 0}, {"Performance", 0}, {"Persuasion", 0}}}}, {dagger}, {leatherHelmet, leatherChestplate, leatherGloves, leatherLeggings, leatherBoots}, false, {}, 0 };
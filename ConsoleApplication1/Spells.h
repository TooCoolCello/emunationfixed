#pragma once
#include <string>	
#include <vector>
#include <map>
#include "Types.h"
#include "Party.h"

enum class savingThrows {
	STRENGTH,
	DEXTERITY,
	CONSTITUTION,
	INTELLIGENCE,
	WISDOM,
	CHARISMA,
	NA,
};
enum class spellLevel {
	FIRST,
	SECOND,
	THIRD,
	FOURTH,
	FIFTH,
	SIXTH,
	SEVENTH,
	EIGHTH,
	NINETH,
	PHILSWIFT,
};
enum class castTimes {
	BONUSACTION,
	REACTION,
	ACTION,
};

struct spell {
	std::string name;
	spellLevel level;
	std::vector<double> effectAmount;
	damageTypes effect;
	std::vector<savingThrows> savingThrow;
	castTimes time;
	statusEffects statusEfffects;
};

// Healing
spell cureWounds = { "Cure Wounds", spellLevel::FIRST, {1,8}, damageTypes::HEAL, {}, castTimes::ACTION, statusEffects::NA };
spell healingWord = { "Healing Word", spellLevel::FIRST, {2,4}, damageTypes::HEAL, {}, castTimes::ACTION, statusEffects::NA };

// Attack
spell burningHands = { "Burning Hands", spellLevel::FIRST, {3,6}, damageTypes::FIRE, {savingThrows::DEXTERITY}, castTimes::ACTION, statusEffects::NA };
spell catapult = { "Catapult", spellLevel::FIRST, {3,8}, damageTypes::BLUDGEONING, {savingThrows::DEXTERITY}, castTimes::ACTION, statusEffects::NA };
spell armsOfHadar = { "Arms of Hadar", spellLevel::FIRST, {2,6}, damageTypes::NECROTIC, {savingThrows::STRENGTH}, castTimes::ACTION, statusEffects::NA };
spell earthTremor = { "Earth Tremor", spellLevel::FIRST, {1,6}, damageTypes::BLUDGEONING, {savingThrows::DEXTERITY}, castTimes::ACTION, statusEffects::NA };
spell guidingBolt = { "Guiding Bolt", spellLevel::FIRST, {4,6}, damageTypes::RADIANT, {}, castTimes::ACTION, statusEffects::NA };
spell hellishRebuke = { "Hellish Rebuke", spellLevel::FIRST, {2,10}, damageTypes::FIRE, {savingThrows::DEXTERITY}, castTimes::REACTION, statusEffects::NA };
spell magicMissile = { "Magic Missile", spellLevel::FIRST, {1,6}, damageTypes::FORCE, {}, castTimes::ACTION, statusEffects::NA };
spell thunderwave = { "Thunderwave", spellLevel::FIRST, {2,8}, damageTypes::THUNDER, {savingThrows::CONSTITUTION}, castTimes::ACTION, statusEffects::NA };
spell witchBolt = { "Witch Bolt", spellLevel::FIRST, {1,12}, damageTypes::LIGHTING, {}, castTimes::ACTION, statusEffects::NA };
spell chaosBolt = { "Chaos Bolt", spellLevel::FIRST, {2,8, 1,6}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell chromaticOrb = { "Chromatic Orb", spellLevel::FIRST, {3,8}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell fireball = { "Fireball", spellLevel::THIRD, {8,6}, damageTypes::FIRE, {}, castTimes::ACTION, statusEffects::NA };

// Effect
spell animalFriendship = { "Animal Friendship", spellLevel::FIRST, {}, damageTypes::EFFECT, {savingThrows::WISDOM}, castTimes::ACTION, statusEffects::NA };
spell alarm = { "Alarm", spellLevel::FIRST, {}, damageTypes::EFFECT, {}, castTimes::BONUSACTION, statusEffects::NA };
spell absorbElements = { "Absorb Elements", spellLevel::FIRST, {1, 6}, damageTypes::EFFECT, {}, castTimes::REACTION, statusEffects::NA };
spell armorOfAgathys = { "Armor of Agathys", spellLevel::FIRST, {5, 5}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell bane = { "Bane", spellLevel::FIRST, {1, 4}, damageTypes::EFFECT, {savingThrows::CHARISMA}, castTimes::ACTION, statusEffects::NA };
spell bless = { "Bless", spellLevel::FIRST, {1, 4}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell causeFear = { "Cause Fear", spellLevel::FIRST, {}, damageTypes::EFFECT, {savingThrows::WISDOM}, castTimes::ACTION, statusEffects::NA };
spell comprehendLanguages = { "Comprehend Languages", spellLevel::FIRST, {}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell createOrDestroyWater = { "Create or Destroy Water", spellLevel::FIRST, {}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell disguiseSelf = { "Disguise Self", spellLevel::FIRST, {}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell divineFavor = { "Divine Favor", spellLevel::FIRST, {}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::NA };
spell faerieFire = { "Faerie Fire", spellLevel::FIRST, {}, damageTypes::EFFECT, {savingThrows::DEXTERITY}, castTimes::ACTION, statusEffects::NA };
spell searingSmite = { "Searing Smite", spellLevel::FIRST, {1,6}, damageTypes::EFFECT, {}, castTimes::ACTION, statusEffects::BURNING };
spell thunderousSmite = { "Thunderous Smite", spellLevel::FIRST, {2,6}, damageTypes::THUNDER, {savingThrows::STRENGTH}, castTimes::BONUSACTION, statusEffects::PRONE };
spell wrathfulSmite = { "Wrathful Smite", spellLevel::FIRST, {1,6}, damageTypes::PSYCHIC, {savingThrows::WISDOM}, castTimes::BONUSACTION, statusEffects::FRIGHTENED };

spell flexTape = { "Flex Tape", spellLevel::PHILSWIFT, {5, 20}, damageTypes::FORCE, {}, castTimes::BONUSACTION, statusEffects::EXHAUSTION };

std::vector<std::vector<spell>> bardSpells{
	// First Level Spells:
	{
		cureWounds, earthTremor, thunderwave, //bane,
},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> clericSpells{
	// First Level Spells:
	{
		cureWounds, guidingBolt, //bane,
},
// Second Level Spells:
{

},
// Second Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> druidSpells{
	// First Level Spells:
	{
		cureWounds, earthTremor, thunderwave, //absorbElements,
},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> paladinSpells{
	// First Level Spells:
	{
		cureWounds, divineFavor, thunderousSmite, wrathfulSmite,
},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> rangerSpells{
	// First Level Spells:
	{
		cureWounds, //absorbElements,
},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> sorcerorSpells{
	// First Level Spells:
	{
		earthTremor, magicMissile, thunderwave, witchBolt, //absorbElements,
},
// Second Level Spells:
{

},
// Third Level Spells:
{
	fireball,
},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> warlockSpells{
	// First Level Spells:
	{
		 armsOfHadar, hellishRebuke, witchBolt, armorOfAgathys,
},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};
std::vector<std::vector<spell>> wizardSpells{
	// First Level Spells:
	{
	   earthTremor, magicMissile, thunderwave, witchBolt, //absorbElements,
},
// Second Level Spells:
{
	
},
// Third Level Spells:
{
	fireball,
},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
// Sixth Level Spells:
{

},
// Seventh Level Spells:
{

},
// Eighth Level Spells:
{

},
// Ninth Level Spells:
{

},
};;
std::vector<spell> philSwiftSpells{
	flexTape,
};



//Otherworldly Patron Spells
std::vector<std::vector<spell>> archfeyOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> celestialOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> fiendOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{
	fireball,
},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> greatOldOneOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> hexbladeOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> ravenQueenOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> seekerOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<spell>> undyingOP{
	// First Level Spells:
{

},
// Second Level Spells:
{

},
// Third Level Spells:
{

},
// Forth Level Spells:
{

},
// Fifth Level Spells:
{

},
};
std::vector<std::vector<std::vector<spell>>> OtherworldlyPatrons{
	archfeyOP, celestialOP, fiendOP, greatOldOneOP, hexbladeOP, ravenQueenOP, seekerOP, undyingOP
};

std::vector<std::vector<std::vector<spell>>> allSpells{
	bardSpells, clericSpells, druidSpells,	paladinSpells, rangerSpells, sorcerorSpells, warlockSpells, wizardSpells
};

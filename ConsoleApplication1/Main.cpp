#include "pch.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <array>
#include <string>
#include <random>
#include <ctime>
#include <cmath>
#include <chrono>
#include <thread>
#include <algorithm>
#include <random>
#include "Armor.h"
#include "Weapons.h"
#include "Party.h"
#include "Emus.h"
#include "Spells.h"

//Party Spell Slots
std::vector<int> calcWarlockSlots(partyMember member) {
	int spellSlots = 0;
	int slotLevel = (member.level <= 9) ? (1 + int(std::trunc((member.level - 1) / 2))) : (5);
	if (member.level == 1) {
		spellSlots = 1;
	}
	else if (member.level <= 10) {
		spellSlots = 2;
	}
	else if (member.level <= 16) {
		spellSlots = 3;
	}
	else {
		spellSlots = 4;
	}
	return std::vector<int> { spellSlots, slotLevel };
};
std::vector<int> calcFullCasterSlots(partyMember member) {
	std::vector<int> spellSlotLevels{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	spellSlotLevels[0] = (member.level <= 2) ? ((member.level == 1) ? (2) : (3)) : (4);
	spellSlotLevels[1] = (member.level >= 3) ? ((member.level == 3) ? (2) : (3)) : (0);
	spellSlotLevels[2] = (member.level >= 5) ? ((member.level == 5) ? (2) : (3)) : (0);
	spellSlotLevels[3] = (member.level >= 7) ? ((member.level == 7) ? (1) : ((member.level == 8) ? (2) : (3))) : (0);
	spellSlotLevels[4] = (member.level >= 9) ? ((member.level == 9) ? (1) : ((member.level < 18) ? (2) : (3))) : (0);
	spellSlotLevels[5] = (member.level >= 11) ? ((member.level < 19) ? (1) : (2)) : (0);
	spellSlotLevels[6] = (member.level >= 13) ? ((member.level < 20) ? (1) : (2)) : (0);
	spellSlotLevels[7] = (member.level >= 15) ? (1) : (0);
	spellSlotLevels[8] = (member.level >= 17) ? (1) : (0);
	return spellSlotLevels;
};
std::vector<int> calcHalfCasterSlots(partyMember member) {
	std::vector<int> spellSlotLevels{ 0, 0, 0, 0, 0 };
	spellSlotLevels[0] = (member.level >= 2) ? ((member.level == 2) ? (2) : (member.level < 5) ? (3) : (4)) : (0);
	spellSlotLevels[1] = (member.level >= 5) ? ((member.level < 7) ? (2) : (3)) : (0);
	spellSlotLevels[2] = (member.level >= 9) ? ((member.level < 11) ? (2) : (3)) : (0);
	spellSlotLevels[3] = (member.level >= 13) ? ((member.level < 15) ? (1) : (member.level < 17) ? (2) : (3)) : (0);
	spellSlotLevels[4] = (member.level >= 17) ? ((member.level < 19) ? (1) : (2)) : (0);
	return spellSlotLevels;
}

//Emu Spell Slots
std::vector<int> calcEmuWarlockSlots(emu member) {
	int spellSlots = 0;
	int slotLevel = (member.level <= 9) ? (1 + int(std::trunc((member.level - 1) / 2))) : (5);
	if (member.level == 1) {
		spellSlots = 1;
	}
	else if (member.level <= 10) {
		spellSlots = 2;
	}
	else if (member.level <= 16) {
		spellSlots = 3;
	}
	else {
		spellSlots = 4;
	}
	return std::vector<int> { spellSlots, slotLevel };
};
std::vector<int> calcEmuFullCasterSlots(emu member) {
	std::vector<int> spellSlotLevels{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	spellSlotLevels[0] = (member.level <= 2) ? ((member.level == 1) ? (2) : (3)) : (4);
	spellSlotLevels[1] = (member.level >= 3) ? ((member.level == 3) ? (2) : (3)) : (0);
	spellSlotLevels[2] = (member.level >= 5) ? ((member.level == 5) ? (2) : (3)) : (0);
	spellSlotLevels[3] = (member.level >= 7) ? ((member.level == 7) ? (1) : ((member.level == 8) ? (2) : (3))) : (0);
	spellSlotLevels[4] = (member.level >= 9) ? ((member.level == 9) ? (1) : ((member.level < 18) ? (2) : (3))) : (0);
	spellSlotLevels[5] = (member.level >= 11) ? ((member.level < 19) ? (1) : (2)) : (0);
	spellSlotLevels[6] = (member.level >= 13) ? ((member.level < 20) ? (1) : (2)) : (0);
	spellSlotLevels[7] = (member.level >= 15) ? (1) : (0);
	spellSlotLevels[8] = (member.level >= 17) ? (1) : (0);
	return spellSlotLevels;
};
std::vector<int> calcEmuHalfCasterSlots(emu member) {
	std::vector<int> spellSlotLevels{ 0, 0, 0, 0, 0 };
	spellSlotLevels[0] = (member.level >= 2) ? ((member.level == 2) ? (2) : (member.level < 5) ? (3) : (4)) : (0);
	spellSlotLevels[1] = (member.level >= 5) ? ((member.level < 7) ? (2) : (3)) : (0);
	spellSlotLevels[2] = (member.level >= 9) ? ((member.level < 11) ? (2) : (3)) : (0);
	spellSlotLevels[3] = (member.level >= 13) ? ((member.level < 15) ? (1) : (member.level < 17) ? (2) : (3)) : (0);
	spellSlotLevels[4] = (member.level >= 17) ? ((member.level < 19) ? (1) : (2)) : (0);
	return spellSlotLevels;
}

//Random Numbers
int mersenneTwister(int lowRoll, int highRoll) {
	int roll = (rand() % highRoll) + lowRoll;
	return roll;
}
double rollDie(int Die) {
	double highRoll = 0;
	switch (Die) {
	case 20: {
		highRoll = 21;
		break;
	}
	case 12: {
		highRoll = 13;
		break;
	}
	case 10: {
		highRoll = 11;
		break;
	}
	case 8: {
		highRoll = 9;
		break;
	}
	case 6: {
		highRoll = 7;
		break;
	}
	case 4: {
		highRoll = 5;
		break;
	}
	}
	return mersenneTwister(1, highRoll);
}

//Calculate Party
void calcHP(partyMember member) {
	switch (member.playerClass) {
	case classes::BARBARIAN:
		if (member.level == 1) {
			member.hp += 12 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(12) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::BARD:
		if (member.level == 1) {
			member.hp += 8 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(8) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::CLERIC:
		if (member.level == 1) {
			member.hp += 8 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(8) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::DRUID:
		if (member.level == 1) {
			member.hp += 8 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(8) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::FIGHTER:
		if (member.level == 1) {
			member.hp += 10 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(10) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::MONK:
		if (member.level == 1) {
			member.hp += 8 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(8) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::PALADIN:
		if (member.level == 1) {
			member.hp += 10 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(10) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::RANGER:
		if (member.level == 1) {
			member.hp += 10 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(10) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::ROGUE:
		if (member.level == 1) {
			member.hp += 8 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(8) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::SHIELDMASTER:
		if (member.level == 1) {
			member.hp += 12 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(12) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::SORCEROR:
		if (member.level == 1) {
			member.hp += 6 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(6) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::WARLOCK:
		if (member.level == 1) {
			member.hp += 8 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(8) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	case classes::WIZARD:
		if (member.level == 1) {
			member.hp += 6 + member.proficiency["Constitution"]["Constitution"];
		}
		else {
			member.hp += rollDie(6) + member.proficiency["Constitution"]["Constitution"];
		}
		break;
	}
}
double calcAP(partyMember member) {
	double totalAP;
	totalAP = 0;
	for (int i = 0; i < 5; i = i + 1) {
		totalAP += member.equippedArmor[i].armorPoints;
	};
	if (member.isShieldEquipped) {
		if (member.playerClass == classes::SHIELDMASTER) {
			totalAP += member.equippedShield[0].armorPoints * 2;
		}
		else {
			totalAP += member.equippedShield[0].armorPoints;
		};
	}
	if (totalAP > 20) {
		totalAP = 20;
	}
	return totalAP;
}
double calcSpell(partyMember member) {
	std::cout << "WIP!";
	int test;
	test = 1;
	return test;
}
double calcAtk(partyMember member) {
	double totalAtk;
	totalAtk = 0;
	bool crit;
	crit = false;
	int dieRolls;
	dieRolls = 1;
	for (int i = 0; i < member.equippedWeapons.size(); i = i + 1) {
		int dieRolled = rollDie(20);
		std::cout << dieRolled << std::endl;
		if (dieRolled == 1) {
			dieRolls = 0;
			std::cout << "You missed!" << std::endl;
		}
		else if (dieRolled >= member.equippedWeapons[0].critChance) {
			dieRolls = 2;
			std::cout << "It was a critical hit!" << std::endl;
		}
		else {
			dieRolls = 1;
		}
		for (int j = 0; j < member.equippedWeapons[i].damage[0] * dieRolls; j = j + 1) {
			totalAtk = totalAtk + rollDie(member.equippedWeapons[i].damage[1]);
		}
	}
	return totalAtk;
}

//Calculate Emu Party
std::vector<spell> chooseEmuSpells(emu member) {
	std::vector<spell> choosableEmuSpells;
	std::vector<spell> chosenEmuSpells;
	if (member.emuClass == classes::WARLOCK) {
		std::vector<int> warlockSlots = calcEmuWarlockSlots(member);
		int OP = mersenneTwister(0, OtherworldlyPatrons.size() - 1);
		for (int i = 0; i < warlockSlots[1]; i++) {
			for (int j = 0; j < warlockSpells[i].size(); j++) {
				choosableEmuSpells.push_back(warlockSpells[i][j]);
			}
			for (int k = 0; k < OtherworldlyPatrons[OP][i].size(); k++) {
				choosableEmuSpells.push_back(OtherworldlyPatrons[OP][i][k]);
			}
		}
		for (int i = 0; i < warlockSlots[0]; i++) {
			int random = mersenneTwister(0, choosableEmuSpells.size() - 1);
			chosenEmuSpells.push_back(choosableEmuSpells[random]);
			choosableEmuSpells.erase(choosableEmuSpells.begin() + random);
		}
	}
	else if (member.emuClass == classes::BARD || member.emuClass == classes::CLERIC || member.emuClass == classes::DRUID || member.emuClass == classes::SORCEROR || member.emuClass == classes::WIZARD) {
		int vectorIndex = 0;
		switch (member.emuClass) {
		case classes::BARD:
			vectorIndex = 0;
			break;
		case classes::CLERIC:
			vectorIndex = 1;
			break;
		case classes::DRUID:
			vectorIndex = 2;
			break;
		case classes::SORCEROR:
			vectorIndex = 5;
			break;
		case classes::WIZARD:
			vectorIndex = 7;
			break;
		}
		std::vector<int> fullCasterSlots = calcEmuFullCasterSlots(member);
		for (int i = 0; i < 9; i++) {
			if (fullCasterSlots[i] != 0) {
				choosableEmuSpells = allSpells[vectorIndex][i];
				for (int j = 0; j < fullCasterSlots[i]; j++) {
					int random = mersenneTwister(0, choosableEmuSpells.size());
					chosenEmuSpells.push_back(choosableEmuSpells[random]);
					choosableEmuSpells.erase(choosableEmuSpells.begin() + random);
				}
			}
		}
	}
	else if (member.emuClass == classes::PALADIN || member.emuClass == classes::RANGER) {
		int vectorIndex = 0;
		switch (member.emuClass) {
		case classes::PALADIN:
			vectorIndex = 3;
			break;
		case classes::RANGER:
			vectorIndex = 4;
			break;
		}
		std::vector<int> halfCasterSlots = calcEmuHalfCasterSlots(member);
		for (int i = 0; i < 5; i++) {
			if (halfCasterSlots[i] != 0) {
				choosableEmuSpells = allSpells[vectorIndex][i];
				for (int j = 0; j < halfCasterSlots[i]; j++) {
					int random = mersenneTwister(0, choosableEmuSpells.size());
					chosenEmuSpells.push_back(choosableEmuSpells[random]);
					choosableEmuSpells.erase(choosableEmuSpells.begin() + random);
				}
			}
		}
	}
	return chosenEmuSpells;
}
double calcEmuAtk(spell attack, emu member) {
	double totalEmuAtk;
	totalEmuAtk = 0;
	int dieRolls;
	dieRolls = 1;
	if (rollDie(20) >= 17) {
		dieRolls = 2;
		std::cout << "It was a critical hit!" << std::endl;
	}
	return totalEmuAtk;
}

emu chooseOpponent() {
	int emuIndex = mersenneTwister(0, emuVector.size());
	return emuVector[emuIndex];
}
bool combat(partyMember member) {
	emu opponent = chooseOpponent();
	std::vector<spell> opponentSpells = chooseEmuSpells(opponent);
	int emuInit = 0;
	int partyInit = 0;
	std::cout << member.hp;
	while (emuInit == partyInit) {
		emuInit = rollDie(20) + int(((opponent.emuStats["Dexterity"] - 10) / 2));
		partyInit = rollDie(20) + int(((member.stats["Dexterity"]["Dexterity"] - 10) / 2));
	}
	if (emuInit > partyInit){
		spell chosenSpell = opponentSpells[0];
		int removeIndex = 0;
		for (int i = 1; i < opponentSpells.size(); i++) {
			if (chosenSpell.effectAmount[0] * chosenSpell.effectAmount[1] < opponentSpells[i].effectAmount[0] * opponentSpells[i].effectAmount[1]) {
				chosenSpell = opponentSpells[i];
				removeIndex = i;
			};
		}
		std::cout << opponent.name << " cast " << chosenSpell.name << ".";
		for (h)
		member.hp - 
	}
	else {
		std::cout << member.name << " cried out in fear, but was suddenly silenced.";
	}
	return true;
}

int main() {
	srand(time(0));
	for (int i = 0; i < 10; i++) {
		std::cout << mersenneTwister(1, 20) << std::endl;
	}
}
